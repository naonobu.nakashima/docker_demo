from flask import Flask, request, json, jsonify
from datetime import datetime
from pytz import timezone
from pymongo import MongoClient
import json
import ast
from bson import json_util
from bson.objectid import ObjectId

client = MongoClient("mongodb://db:27017")
db = client.projectDB
db_usage = db["usage"]

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

usage = {}

@app.route('/hello')
def hello_world():
    return jsonify({'message': 'Hello World'})


@app.route('/usage', methods=['GET'])
def usage_list():
    corsor = db_usage.find()
    rtn_msg = []
    for doc in corsor :
        json_doc = json.dumps(doc, default=json_util.default)
        rtn_msg.append(json_doc)
    return jsonify(rtn_msg)

@app.route('/usage/<int:id>', methods=['GET'])
def usage_detail(id):
    pass

@app.route('/usage', methods=['POST'])
def usage_create():
    # tmp_time = datetime.now(timezone('Asia/Tokyo')) 
    req = ast.literal_eval(request.get_json())
    print(req)
    if 'cpu' in req and 'mem' in req and 'hostname' in req and 'time' in req:
        db_usage.insert({
            'time': datetime.fromisoformat(req['time']),
            'hostname': req['hostname'],
            'cpu': req['cpu'],
            'mem': req['mem']  
        })
        msg = 'New usage is recorded.'
    else:
        msg = 'No usage is recorded.'
    rtn_msg  = {
        'message': msg,
        'data': {
            'time': datetime.fromisoformat(req['time']),
            'hostname': req['hostname'],
            'cpu': req['cpu'],
            'mem': req['mem']            
        }
    }
    return jsonify(rtn_msg )
    

@app.route('/usage/<int:id>', methods=['DELETE'])
def usage_delete(id):
    pass
